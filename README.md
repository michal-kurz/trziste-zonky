# Tržiště Zonky
This project is a test assignment by [U+](https://u.plus/)

There's a demo running here: https://ernsthess.gitlab.io/trziste-zonky/

## The assignment (Czech)
Naprogramuj single-page aplikaci, která bude každých 5 minut kontrolovat nové půjčky na tržišti zonky.cz a vypíše je do seznamu.  
Seznam půjček bude obsahovat obrázek, jméno a příběh omezený na 20 slov s celkovou maximální délkou příběhu 200 znaků.  
Seznam půjček bude možné řadit podle délky trvání, ratingu, požadované částky a deadlinu.  
Každou z půjček si uživatel může rozkliknout, aby viděl její detail se všemi údaji z API.  
Pro vývoj aplikace použij React  
K aplikaci napiš alespoň dva testy  
hotovou věc někam nasaď (svuj server, heroku...etc)

API najdeš na adrese: https://api.zonky.cz/loans/marketplace

## Anticipated questions

#### Why separate local state into it's own component - e.g. [LoanCardStateContainer.jsx](https://gitlab.com/ErnstHess/trziste-zonky/blob/master/src%2Fcomponents%2FLoanCard%2FLoanCardStateContainer.jsx) ?

The short answer is [**SRP**](https://en.wikipedia.org/wiki/Single_responsibility_principle). It makes the code easier to read, maintain and to re-use, as the component is purely presentational and the logic is encapsulated in it's own component. 

Future changes to the app state structure become much easier. Want to lift the state up? Just ditch the container. Moving state to redux? Just swap the container for a redux one.

#### Why Flow and not TypeScript
I already used TypeScript in [this test project](https://gitlab.com/ErnstHess/currency-converter), so I wanted to use Flow for a change so I have both covered :)

**If you have any other questions/observations, please email me!**
