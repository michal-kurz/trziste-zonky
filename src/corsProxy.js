// @flow
const PROXY_URL_BASE = 'https://api.codetabs.com/v1/proxy?quest='
const EXTRACT_ORIGINAL_RESPONSE_FROM_PROXY_RESPONSE = (proxyResponse: object): object => proxyResponse.data


export const proxifyUrl = (url: string): string => `${PROXY_URL_BASE}${url}`
export const unpackProxyResponse = EXTRACT_ORIGINAL_RESPONSE_FROM_PROXY_RESPONSE
