// @flow
/**
 * A sorting function used as callback for array.sort()
 */
export type SorterType = (any, any) => number

/**
 * On object of various sorter types
 */
export type SortersType = {
	/**
	 * A sorting function used as callback for array.sort()
	 */
	[key: string]: SorterType,
}
