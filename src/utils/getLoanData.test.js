import { getLoanData } from '.'
import get from 'lodash/fp/get'

import { orderFilters } from '../components/Marketplace/filters'

const SORTING_PROPERTIES = orderFilters.map(filter => get('propName', filter))
const REQUIRED_PROPERTIES = [
	'name',
	'photos[0].url',
	'story',
	...SORTING_PROPERTIES,
]

let fetchedData

beforeAll(() => {
	return getLoanData().then(data => {
		fetchedData = data
	})
})

describe('getLoanData provides valid loan data', () => {
	it('it returns an array', () => {
		expect(Array.isArray(fetchedData)).toBe(true)
	})
	
	describe('returned array has the correct shape', () => {
		it('has objects only', () => {
			fetchedData.forEach(arrayElement => {
				expect(typeof arrayElement).toBe('object')
			})
		})
		
		test('all objects have required properties', () => {
			fetchedData.forEach(arrayElement => {
				REQUIRED_PROPERTIES.forEach(propertyGetter => {
					const property = get(propertyGetter, arrayElement)
					expect(property).not.toBeUndefined()
				})
			})
		})
	})
})
