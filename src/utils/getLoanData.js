// @flow
import axios from 'axios'
import { proxifyUrl, unpackProxyResponse } from '../corsProxy'

const API_URL = 'https://api.zonky.cz/loans/marketplace'

export default async () => {
	const url = proxifyUrl(API_URL)
	const proxyResponse = await axios.get(url)
	return unpackProxyResponse(proxyResponse)
}
