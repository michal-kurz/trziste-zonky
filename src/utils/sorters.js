// @flow
import { type SortersType } from './types'


export const sortLoansBy: SortersType = {
	term: (a, b) => a.termInMonths - b.termInMonths,
	rating: (a, b) => a.rating < b.rating ? 1 : -1,
	amount: (a, b) => a.amount - b.amount,
	deadline: (a, b) => new Date(a) - new Date(b),
}
