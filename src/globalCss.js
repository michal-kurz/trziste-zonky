import { css } from 'emotion'

// Global styles for emotion's <Global />
export default `
	* { box-sizing: border-box; }
	body {
		margin: 10px 20px;
		font-family: "Open Sans", sans-serif;
	}
`

export const clearfix = css`
	&:after {
		display: block;
		content: "";
		clear: both;
	}
`
