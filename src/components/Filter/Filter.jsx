// @flow
import React from 'react'
import noop from 'lodash/noop'
import { StyledFilter } from './styles'
import { type FilterPropsType } from './types'


const Filter = ({ label, onClick, active }: FilterPropsType) => (
	<StyledFilter
		onClick={active ? noop : onClick}
		active={active}
	>
		{label}
	</StyledFilter>
)

export default Filter
