// @flow
import styled from '@emotion/styled'

export const StyledFilter = styled.div`
	font-size: 14px;
	padding: 5px;
	border: 2px solid blue;
	border-radius: 5px;
	color: blue;
	margin-right: 10px;
	display: inline-block;
	cursor: pointer;
	text-transform: uppercase;
	
	${({ active }) => active && `
		color: white;
		font-weight: bold;
		background-color: blue;
		cursor: default;
		`
	}
`
