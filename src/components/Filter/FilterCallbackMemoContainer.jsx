// @flow
import React, { useCallback } from 'react'
import Filter from './Filter'
import { type FilterCallbackMemoContainerPropsType } from './types'

const FilterCallbackMemoContainer = ({ setFilter, id, ...rest }: FilterCallbackMemoContainerPropsType) => {
	const onClick = useCallback(
		() => setFilter(id),
		[setFilter, id],
	)
	
	return <Filter onClick={onClick} {...rest} />
}

export default FilterCallbackMemoContainer
