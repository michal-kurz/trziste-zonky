// @flow
export type FilterPropsType = {
	/**
	 * Filter "button text"
	 */
	label: string,
	/**
	 * Filter onclick callback
	 */
	onClick: () => void,
	/**
	 * Active status of the filter - used for css + disables onClick callback
	 */
	active: boolean,
}

export type FilterCallbackMemoContainerPropsType = {
	/**
	 * Filter "button text"
	 */
	label: string,
	/**
	 * Active status of the filter - used for css + disables onClick callback
	 */
	active: boolean,
	/**
	 * Function activating a filter - gets bound to id, memoized and sent to Filter as onClick
	 *    @param {any} id if the filter
	 */
	setFilter: (id: any)=>void,
	/**
	 * Id of the filter - passed to setFilter
	 */
	id: any,
}
