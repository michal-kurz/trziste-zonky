// @flow
import React from 'react'
import Filter from '../Filter'
import { StyledH2, StyledFilterGroup } from './styles'
import { type FilterListPropsType } from './types'

const FilterGroup = ({
	filters,
	activeFilter,
	setFilter,
	label,
}: FilterListPropsType) => (
	<StyledFilterGroup>
		<StyledH2>{label}</StyledH2>
		{filters.map(filter => (
			<Filter
				key={filter.id}
				setFilter={setFilter}
				id={filter.id}
				label={filter.label}
				active={activeFilter === filter.id}
			/>
		))}
	</StyledFilterGroup>
)

export default FilterGroup
