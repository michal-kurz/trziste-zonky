// @flow
type FilterType = {
	/**
	 * Id of the filter within it's group = filters array
	 */
	id: any,
	/**
	 * Label displayed on Filter
	 */
	label: string,
	[key: string]: any,
}

export type FilterListPropsType = {
	/**
	 * Filters to render, each contains id and label
	 * Array of FilterType - { id: any, label: string }
	 */
	filters: FilterType[],
	/**
	 * Active filter within the filter group
	 */
	activeFilter: any,
	/**
	 * Activates a filter within the group
	 * @param {any} id of the filter to activate
	 */
	setFilter: (id: any) => void,
	/**
	 * Label describing the filter group
	 */
	label: string,
}
