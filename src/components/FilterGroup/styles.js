// @flow
import styled from '@emotion/styled'

export const StyledH2 = styled.h2`
	display: inline-block;
	margin: 8px 15px 8px 0;
	font-size: 26px;
	color: darkblue;
	text-transform: uppercase;
`

export const StyledFilterGroup = styled.div`
	display: inline-flex;
	margin-right: 40px;
	align-items: center;
`
