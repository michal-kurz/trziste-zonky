// @flow
import React, { useState } from 'react'
import Marketplace from './Marketplace'
import { orderFilters, directionFilters } from './filters'
import {
	type MarketplaceStateContainerPropsType,
	type sortLoansType,
} from './types'

const DEFAULT_ORDER_FILTER = orderFilters[0].id
const DEFAULT_DIRECTION_FILTER = directionFilters[0].id

const sortLoans: sortLoansType = (loanData, activeOrderFilterId, activeDirectionFilterId) => {
	if (!loanData) return null	// If data not yet ready
	
	const activeOrderFilter = orderFilters.find(filter => filter.id === activeOrderFilterId)
	if (activeOrderFilter === undefined) {
		console.warn('Order filter with id', activeOrderFilterId, 'not found. Proceeding without sorting loanData! order filters:', orderFilters)
		return loanData
	}
	
	const sortedLoans = loanData
		.slice()	// Avoid mutation
		.sort(activeOrderFilter.sorter)
	
	const orderedLoans = activeDirectionFilterId
		? sortedLoans
		: sortedLoans
			.slice()	// Avoid mutation
			.reverse()
	
	return orderedLoans
}

const MarketplaceStateContainer = ({
	loanData,
	...rest
}: MarketplaceStateContainerPropsType) => {
	
	const [activeOrderFilter, setOrderFilter] = useState(DEFAULT_ORDER_FILTER)
	const [activeDirectionFilter, setDirectionFilter] = useState(DEFAULT_DIRECTION_FILTER)
	
	const orderedLoans = sortLoans(loanData, activeOrderFilter, activeDirectionFilter)
	
	return (
		<Marketplace
			activeOrderFilter={activeOrderFilter}
			setOrderFilter={setOrderFilter}
			activeDirectionFilter={activeDirectionFilter}
			setDirectionFilter={setDirectionFilter}
			loanData={orderedLoans}
			{...rest}
		/>
	)
}

export default MarketplaceStateContainer
