// @flow
import * as React from 'react'
import { type LoanDataType } from '../LoanCard/types'

export type LoanDataArrayType = ?LoanDataType[]

/* =============== filters.js =============== */
/**
 * A sorting function used as callback for array.sort()
 */
type GeneralFilterType = {
	/**
	 * Id of the filter (must be unique in group)
	 */
	id: any,
	/**
	 * Text label of the filter - to be rendered in browser
	 */
	label: string,
}

export type OrderFilterType = {
	...$Exact<GeneralFilterType>,
	/**
	 * Name of the prop on loan data object - used
	 * as sorting criteria when filter is active
	 */
	propName: string,
	/**
	 * A callback for array.sort(), determining
	 * the sorting algorithm for the given field
	 */
	sorter: (any, any) => number,
}

export type OrderFiltersType = OrderFilterType[]

export type DirectionFilterType = {
	...$Exact<GeneralFilterType>,
}

export type DirectionFiltersType = DirectionFilterType[]

/* =============== Marketplace.jsx =============== */
export type MarketplacePropsType = {
	/**
	 * Array of data for all the loan cases
	 */
	loanData: LoanDataArrayType,
	/**
	 * Active filter id specifying which attribute loans get sorted by
	 */
	activeOrderFilter: any,
	/**
	 * Sets a filter as active
	 * @param {any} filter id
	 */
	setOrderFilter: (id: any) => void,
	/**
	 * Active filter id specifying if loans are sorted by normal or reverse order
	 */
	activeDirectionFilter: any,
	/**
	 * Sets a filter as active
	 * @param {any} filter id
	 */
	setDirectionFilter: (id: any) => void,
}

/* =============== MarketplaceStateContainer.jsx =============== */
export type sortLoansType = (
	/**
	 * Array of data for all the loan cases
	 */
	loanData: LoanDataArrayType,
	/**
	 * Active filter id specifying which attribute loans get sorted by
	 */
	activeOrderFilterId: any,
	/**
	 * Active filter id specifying if loans are sorted by normal or reverse order
	 */
	activeDirectionFilterId: any,
) => LoanDataArrayType | null

export type MarketplaceStateContainerPropsType = {
	/**
	 * Array of data for all the loan cases
	 */
	loanData: LoanDataArrayType,
}

/* =============== withLoanData.jsx =============== */
export type injectedPropsType = {|
	/**
	 * Array of sorted data for all the loan cases
	 */
	loanData: LoanDataArrayType
|}

export type withLoanDataStateType = {|
	/**
	 * Array of data for all the loan cases
	 */
	allLoans: LoanDataArrayType
|}



