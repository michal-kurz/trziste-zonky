// @flow
import Marketplace from './MarketplaceStateContainer'
import withLoanData from './withLoanData'

export default withLoanData(Marketplace)
