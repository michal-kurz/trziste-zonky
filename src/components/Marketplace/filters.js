// @flow
import { sortLoansBy } from '../../utils/sorters'
import { type OrderFiltersType, type DirectionFiltersType } from './types'

export const orderFilters: OrderFiltersType = [
	{ id: 0, propName: 'termInMonths', label: 'term in months', sorter: sortLoansBy.term },
	{ id: 1, propName: 'rating', label: 'rating', sorter: sortLoansBy.rating },
	{ id: 2, propName: 'amount', label: 'amount', sorter: sortLoansBy.amount },
	{ id: 3, propName: 'deadline', label: 'deadline', sorter: sortLoansBy.deadline },
]

export const directionFilters: DirectionFiltersType = [
	{ id: 0, label: 'Default' },
	{ id: 1, label: 'Reverse' },
]
