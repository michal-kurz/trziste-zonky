// @flow
import * as React from 'react'
import { getLoanData } from '../../utils'
import { type injectedPropsType, type withLoanDataStateType } from './types'

const UPDATE_INTERVAL = 1000 * 60 * 5   // 5 minutes

const withLoanData = <Config: {}>(
	Component: React.ComponentType<Config>,
): React.ComponentType<$Diff<Config, injectedPropsType>> => {
	class LoanDataProvider extends React.PureComponent<Config, withLoanDataStateType> {
		setState: (?withLoanDataStateType | ((withLoanDataStateType, Config) => ?withLoanDataStateType))=>void
		interval: IntervalID
		updateLoanList: ()=>void
		
		constructor(props) {
			super(props)
			this.state = {
				allLoans: null,
			}
			
			this.updateLoanList = this.updateLoanList.bind(this)
			this.setState = this.setState.bind(this)
		}
		
		componentDidMount() {
			this.updateLoanList()
			this.interval = setInterval(this.updateLoanList, UPDATE_INTERVAL)
		}
		
		componentWillUnmount() {
			clearInterval(this.interval)
		}
		
		updateLoanList() {
			const { setState } = this
			
			getLoanData()
				.then(loanData => setState({ allLoans: loanData }))
				.catch(console.warn)
		}
		
		render() {
			return <Component loanData={this.state.allLoans} {...this.props} />
		}
		
	}
	
	return LoanDataProvider
}

export default withLoanData
