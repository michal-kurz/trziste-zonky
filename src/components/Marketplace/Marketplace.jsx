// @flow
import React from 'react'
import LoanGrid from '../LoanGrid'
import FilterGroup from '../FilterGroup'
import { orderFilters, directionFilters } from './filters'
import { type MarketplacePropsType } from './types'

const Marketplace = ({
	loanData,
	activeOrderFilter,
	setOrderFilter,
	activeDirectionFilter,
	setDirectionFilter,
}: MarketplacePropsType) => {
	if (!loanData) return 'Waiting for data...'
	
	return (
		<>
			<FilterGroup
				label={'Filter by:'}
				filters={orderFilters}
				activeFilter={activeOrderFilter}
				setFilter={setOrderFilter}
			/>
			<FilterGroup
				label={'Order:'}
				filters={directionFilters}
				activeFilter={activeDirectionFilter}
				setFilter={setDirectionFilter}
			/>
			<LoanGrid loans={loanData} />
		</>
	)
	
}

export default Marketplace
