// @flow
import { type LoanPreviewPropsType } from './LoanPreview/types'
import { type LoanDetailPropsType } from './LoanDetail/types'

/**
 * Data for the loan case
 */
export type LoanDataType = {
	// Technically LoanPreviewPropsType would suffice as it's a superset of
	// LoanDetailPropsType, but I think it's good practice to include all
	...$Exact<LoanDetailPropsType>,
	...$Exact<LoanPreviewPropsType>,
}

export type LoanCardPropsType = {
	/**
	 * If open, LoanCard displays <LoanDetail />, otherwise <LoanPreview />
	 */
	isOpen: boolean,
	/**
	 * Bound callback that toggles isOpen
	 */
	toggleOpenState: ()=>void,
	/**
	 * Data for the loan case
	 */
	data: LoanDataType,
}

export type LoanCardStateContainerPropsType = {
	/**
	 * Data for the loan case
	 */
	data: LoanDataType,
}
