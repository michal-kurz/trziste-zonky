// @flow
import React, { useState, useCallback } from 'react'
import LoanCard from './LoanCard'
import { type LoanCardStateContainerPropsType } from './types'

function LoanCardStateContainer({ ...rest }: LoanCardStateContainerPropsType) {
	const [isOpen, setOpenState] = useState(false)
	const toggleOpenState = useCallback(() => setOpenState(!isOpen), [isOpen])
	
	return (
		<LoanCard
			isOpen={isOpen}
			toggleOpenState={toggleOpenState}
			{...rest}
		/>
	)
}

export default LoanCardStateContainer
