// @flow
import React from 'react'
import { StyledH2, StyledEntry, StyledEntryLabel } from './styles'
import { type LoanDetailPropsType } from './types'

const propToString = (prop: any): string => {
	switch (typeof (prop)) {
		case 'boolean':
			return prop
		case 'number':
			return prop
		case 'string':
			return prop
		case 'object':
			return JSON.stringify(prop)
		default:
			console.warn(`Prop of type ${typeof prop} is not accounted for`)
			return prop
	}
}


const LoanDetail = ({ name, ...rest }: LoanDetailPropsType) => (
	<>
		<StyledH2>{name} (detail)</StyledH2>
		{Object.keys(rest).reduce(
			(acc, key) => [
				...acc,
				<StyledEntry key={key}>
					<StyledEntryLabel>{key}:</StyledEntryLabel>
					<div>{propToString(rest[key])}</div>
				</StyledEntry>,
			],
			[],
		)}
	</>
)

export default LoanDetail
