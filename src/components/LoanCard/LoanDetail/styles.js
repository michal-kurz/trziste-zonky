// @flow
import styled from '@emotion/styled'

export const StyledH2 = styled.h2`
	margin: 0 0 10px;
	color: forestgreen;
`
export const StyledEntry = styled.div`
	display: flex;
`

export const StyledEntryLabel = styled.div`
	font-weight: bold;
	margin-right: 10px;
`
