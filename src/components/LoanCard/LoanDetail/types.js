// @flow
export type LoanDetailPropsType = {
	/**
	 * Name of the loan case - come from API
	 */
	name: string,
}
