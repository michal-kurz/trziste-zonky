// @flow
import React from 'react'
import LoanPreview from './LoanPreview'
import LoanDetail from './LoanDetail'
import { StyledLoanCard } from './styles.js'
import { clearfix } from '../../globalCss'
import { type LoanCardPropsType } from './types'

const LoanCard = ({ data, isOpen, toggleOpenState }: LoanCardPropsType) => (
	<StyledLoanCard
		onClick={toggleOpenState}
		className={clearfix}
		active={isOpen}
	>
		{isOpen
			? <LoanDetail {...data} />
			: <LoanPreview
				name={data.name}
				imgSrc={data.photos[0].url}
				story={data.story}
			/>}
	</StyledLoanCard>
)


export default LoanCard
