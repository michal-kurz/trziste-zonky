// @flow
import styled from '@emotion/styled'

export const StyledLoanCard = styled.div`
	cursor: pointer;
	flex-grow: 1;
	border: 3px solid olive;
	margin: 15px;
	padding: 10px;
	width: calc((100% / 3) - 30px);;
	${props => props.active && 'width: 100%'}
`
