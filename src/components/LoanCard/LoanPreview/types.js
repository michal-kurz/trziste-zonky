export type trimStoryType = (story: string) => string

export type LoanPreviewPropsType = {
	/**
	 * Name of the loan case - come from API
	 */
	name: string,
	/**
	 * Cover image for the loan case - comes from API
	 */
	imgSrc: string,
	/**
	 * The story (description) of the loan case - comes from API
	 */
	story: string,
}
