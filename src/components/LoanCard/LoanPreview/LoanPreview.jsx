// @flow
import React, { useMemo } from 'react'
import { StyledImg, StyledH2 } from './styles.js'
import { type LoanPreviewPropsType, type trimStoryType } from './types'

const WORD_LIMIT = 20
const CHARACTER_LIMIT = 200

const trimStory: trimStoryType = story => {
	return story
		.split(' ')
		.slice(0, WORD_LIMIT)
		.join(' ')
		.substring(0, CHARACTER_LIMIT)
}

function LoanPreview({ name, imgSrc, story }: LoanPreviewPropsType) {
	const trimmedStory = useMemo(() => trimStory(story), [story])
	
	return (
		<>
			<StyledImg src={`https://api.zonky.cz${imgSrc}`} alt="Ilustrace" />
			<StyledH2>{name}</StyledH2>
			<p>{trimmedStory}</p>
		</>
	)
}

export default LoanPreview
