// @flow
import styled from '@emotion/styled'

export const StyledImg = styled.img`
	float: left;
	margin-right: 10px;
	height: 100px;
`

export const StyledH2 = styled.h2`
	margin: 0;
	color: forestgreen;
`
