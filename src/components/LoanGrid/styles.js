import styled from '@emotion/styled'

export const StyledLoanGrid = styled.div`
	display: flex;
	justify-content: flex-start;
	flex-direction: row;
	margin: 0 -15px;
	flex-wrap: wrap;
`
