// @flow

// Importing the LoanDataType rather than LoanCardStateContainerPropsType as
// I believe the structure shouldn't depend on a container
import { type LoanDataType } from '../LoanCard/types'

export type LoanGridPropsType = {
	/**
	 * Array of data for all the loan cases
	 */
	loans: LoanDataType[],
}
