// @flow
import * as React from 'react'
import LoanCard from '../LoanCard'
import { type LoanGridPropsType } from './types'
import { StyledLoanGrid } from './styles'

const LoanGrid = ({ loans }: LoanGridPropsType): React.Node => (
	<StyledLoanGrid>
		{loans.map(loan => (
				<LoanCard
					key={loan.id}
					data={loan}
				/>
			),
		)}
	</StyledLoanGrid>
)


export default LoanGrid
