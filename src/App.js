import React from 'react'
import Marketplace from './components/Marketplace'
import { Global, css } from '@emotion/core'
import globalCss from './globalCss'


function App() {
	return (
		<>
			<Marketplace />
			<Global styles={css(globalCss)} />
		</>
	)
}

export default App
